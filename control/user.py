# coding:utf-8
import os
import random

from base import Base
from common.error import CountError


class User(Base):
    def __init__(self, userName, user_json, gift_json):
        self.userName = userName
        super().__init__(user_json, gift_json)
        self.get_user()
        self.gift_random = list(range(1, 101))

    def get_user(self):
        # 调用base的查询方法
        users = self._Base__read_users()
        current_user = users.get(self.userName)
        if current_user == None:
            raise ValueError('该用户 %s 不存在' % self.userName)

        if current_user.get('status') == False:
            raise ValueError('该用户 %s 已被封禁' % self.userName)

        if current_user.get('roles') != 'normal':
            raise ValueError('管理员要有管理员的样子')

        self.user = current_user
        self.role = current_user.get('roles')
        self.name = current_user.get('userName')
        self.status = current_user.get('status')

    def get_gifts(self):
        gifts = self._Base__read_gifts()
        gift_lists = []

        for level_one, level_one_pool in gifts.items():
            for levl_two, level_two_pool in level_one_pool.items():
                for gift_name, gift_info in level_two_pool.items():
                    gift_lists.append(gift_info.get('name'))
        return gift_lists

    def choice_gift(self):
        # 一级奖池 二级奖池
        first_level, second_level = None, None
        # 随机数
        level_one_count = random.choice(self.gift_random)
        # 概率
        if 1 <= level_one_count <= 50:
            first_level = 'level1'
        elif 51 <= level_one_count <= 80:
            first_level = 'level2'
        elif 81 <= level_one_count < 95:
            first_level = 'level3'
        elif level_one_count >= 95:
            first_level = 'level4'
        else:
            raise CountError('level_one_count need 0~100')
        # 查询出所有奖池
        gifts = self._Base__read_gifts()
        # 得到一级奖池
        level_one = gifts.get(first_level)

        # 开始获取二级奖池
        level_two_count = random.choice(self.gift_random)
        # 概率
        if 1 <= level_two_count <= 80:
            second_level = 'level1'
        elif 81 <= level_two_count < 95:
            second_level = 'level2'
        elif 95 <= level_two_count < 100:
            second_level = 'level3'
        else:
            raise CountError('level_two_count need 0~100')

        level_two = level_one.get(second_level)
        if len(level_two) == 0:
            print('不好意思，你个小黑子')
            return

        # 定义一个列表存放奖品
        gift_names = []
        for k, _ in level_two.items():
            gift_names.append(k)

        # 随机列表下标获得奖品名称
        gift_name = random.choice(gift_names)
        # 获取奖品
        gift_info = level_two.get(gift_name)
        # 判断奖品是否还有
        if gift_info.get('count') <= 0:
            print('哎呦，你干嘛~~~~，这都不中~~~')
            return

        self._Base__gift_update(userName=self.userName, first_level=first_level, second_level=second_level,
                                gift_name=gift_info.get('name'))
        print('你居然中了 %s ？蹭蹭欧气！' % gift_name)


if __name__ == '__main__':
    user_path = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))), 'storage', 'user.json')
    gift_path = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))), 'storage', 'gift.json')
    user = User('yiqie', user_path, gift_path)
    i = list(range(1, 10))
    for i in i:
        user.choice_gift()
