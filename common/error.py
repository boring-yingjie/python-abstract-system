# coding:utf-8

# 文件路径不存在
class NotPathError(Exception):
    def __init__(self, msg):
        self.msg = msg


# 文件不是JSON格式
class FormatError(Exception):
    def __init__(self, msg='需要json格式'):
        self.msg = msg


# 不是文件格式
class NotFileError(Exception):
    def __init__(self, msg='这不是文件，请检查好文件格式'):
        self.msg = msg


class UserExistsError(Exception):
    def __init__(self, msg):
        self.msg = msg


class RoleError(Exception):
    def __init__(self, msg):
        self.meg = msg


class LevelError(Exception):
    def __init__(self, msg):
        self.msg = msg


class NegativeNumberError(Exception):
    def __init__(self, msg):
        self.msg = msg


class NotUserError(Exception):
    def __init__(self, msg):
        self.msg = msg


class UserActiveError(Exception):
    def __init__(self, msg):
        self.msg = msg


class CountError(Exception):
    def __init__(self, msg):
        self.msg = msg
