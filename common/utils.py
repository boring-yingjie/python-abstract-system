# coding:utf-8
import os
import time

from .error import NotFileError, NotPathError, FormatError


# 检查json文件的工具类  path:json文件的路径
def check_file(path):
    # json 地址是否存在
    if not os.path.exists(path):
        raise NotPathError(" %s 文件不存在" % path)
        # 是否是json文件
    if not path.endswith('.json'):
        raise FormatError()
        # 是否是文件
    if not os.path.isfile(path):
        raise NotFileError()

def timestamp_to_string(timestamp):
    time_obj = time.localtime(timestamp)
    time_str = time.strftime('%Y-%m-%d %H:%M:%S', time_obj)
    return time_str
